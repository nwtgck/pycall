# Call python methods from other language by BSON

# Dependencies:
#  - pip install bson

import socket
import bson
import traceback
import sys
def start(reciever):
    host = "localhost"
    port = 2345
    FUNC_NAME_KEY = 'func_name'
    ARGUMENTS_KEY = 'arguments'
    RESULT_KEY    = 'result'
    ERROR_KEY     = 'error'

    s = socket.socket()
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((host, port))

    s.listen(1)

    print("Python Call is started")

    while True:
        # --- Start One connection ---
        conn, addr = s.accept()

        while True:
            # Initialize the request BSON as empty
            req_bson = bytes(b'')


            # Get the length of the request BSON
            length_bytes = conn.recv(4)

            # If no data from the client
            if not length_bytes:
                # Close the connection
                conn.close()
                break

            req_length = int.from_bytes(length_bytes, 'big')

            # Get request BSON
            req_bson = conn.recv(req_length)


            try:
                req       = bson.loads(req_bson) # req_bson :: bytes
                print(req)
                func_name = req[FUNC_NAME_KEY]
                arguments = req[ARGUMENTS_KEY]
                result = getattr(reciever, func_name)(*arguments)
            except Exception as exp:
                # Send the error response
                res_bson = bson.dumps({ERROR_KEY: str(exp)})

                # Get a bytes representaion of len(res_bson)
                res_length_bytes = len(res_bson).to_bytes(4, 'big')
                # send the response BSON length
                conn.send(res_length_bytes)

                conn.send(res_bson)
                # Print the stack trace
                traceback.print_exc()
                continue

            # Send the response
            res_bson = bson.dumps({RESULT_KEY: result})

            # Get a bytes representaion of len(res_bson)
            res_length_bytes = len(res_bson).to_bytes(4, 'big')
            # send the response BSON length
            conn.send(res_length_bytes)

            conn.send(res_bson)

        # conn.close()
