# A test of call python methods from other language by BSON

import pycall

class BindSample:
    # test() can be called by other language
    def test(self, a, b, c):
        return a * b * c

bind_sample = BindSample()
pycall.start(bind_sample)
